<img class="center" src="fanuc.jpg" alt="Picture of me">

<h1 class="red"> Welcome! I'm Wilson... </h1>

I am a passionate engineer, developer, and robot enthusiast. I love all things open source, and sharing what I have learned. Some of my interests/skills include: Knowlege of GNU/Linux systems, BASH, Python, C/C++, OpenCV, ROS, Code CAD (Such as OpenSCAD), and many more enterprise grade tools and methodologies.

You can [download my resume here](files/resume-wilsonjholmes.pdf).

<br>

## Featured post
<ul class="articles">
<li><a href="my-journey-learning-ros.html">My Journey Learning ROS ↩︎</a><p class="date">December 26nd, 2021</p></li>
</ul>

This is an excerpt from a paper I wrote for my senior project at Michigan Technological University. I was software lead for the GrowBot robotics team, as well as president of the Open Source Hardware Enterprise (OSHE) at Tech, and that is how I ended up getting an opportunity to learn ROS, web development, and much more. The intent of the following is to showcase what I learned, while also hopefully being an aid to those learning ROS from scratch through wikis and blogs like I did. Hope this helps :)

### See my recent posts

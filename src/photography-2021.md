# A collection of my favorite photos from 2021

<p class="date">January 1st, 2022</p>

![Painesdale, Champion mine shaft number 4](files/PA300572.JPG)
![MTU GLRC](files/PA250386.JPG)

One of my hobbies is photography. I love capturing unique moments of beauty. Here are
some of my favorites from 2021. Feel free to use these photos, but please let me know if you are
doing especially cool things with them :)

## The Cheese factory

![Rocks](files/PA300445.JPG)
![Streaming light](files/PA300463.JPG)
![Photo out a window](files/PA300486.JPG)
![Tree through window](files/PA300496.JPG)
![Ceiling drip and hole](files/PA300508.JPG)
![Plant through peep hole](files/PA300520.JPG)
![Brick wall](files/PA300526.JPG)
![Brick wall grafitti](files/PA300528.JPG)
![Hole in ceiling with tip of tree](files/PA300559.JPG)
![Ceiling growth](files/PA300561.JPG)

### Here it is on the map

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-88.64607453346254%2C47.058794088248064%2C-88.62850069999695%2C47.06610988826697&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=17/47.06245/-88.63729">View Larger Map</a></small>


## The Jordan River Valley (Michigan)

![Stump in frozen stream](files/OMD16015.JPG)
![Twig in frozen stream](files/OMD16034.JPG)
![Frozen grass](files/OMD16035.JPG)
![Frozen branch in stream](files/OMD16058.JPG)
![Hole to river underneath](files/OMD16062.JPG)

### Here it is on the map

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-85.02894401550293%2C44.993333336892874%2C-84.91839408874513%2C45.054060350120416&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=14/45.0237/-84.9737">View Larger Map</a></small>

<!--
## Allouez Michigan

![Frozen sand](files/20211219_115703.jpg)
![Frozen sandy surf](files/20211219_121713.jpg)
![Frozen sand ice shelf](files/20211219_121837.jpg)
![Frozen surf](files/20211219_121934.jpg)
![Frozen mini white-caps](files/20211219_121947.jpg)

###	Here it is on the map

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-88.38273525238039%2C47.37838856860546%2C-88.35329532623292%2C47.40746988329364&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=15/47.3929/-88.3680">View Larger Map</a></small>
-->

#photography #houghton #keweenaw #omdem1 #camera #rocks #s9plus

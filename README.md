Wilsons's Website
=================

This is the repository for my personal website. It is made with the help of ssg6,
lowdown, feather icons, and the Inter font. This website design was heavily inspired by Wolfgang's Channel on YouTube so credits to him for putting me on to this.



Development Setup
-----------------

To get started working `git clone` the repository to your local machine then `cd` into it and run `./setup`. This should update the git submodules, configure and compile lowdown, and then generate the website from the markdown files in `./src`. This website design was heavily inspired by Wolfgang's Channel on YouTube so credits to him for putting me on to this.

Development "Server"
--------------------

Run `./auto-generate.sh`

TODO:
-----

Fix `auto-generate.sh`

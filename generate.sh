#!/bin/sh -e

rm -rf ./public;
rm -rf ./src/tag*.md;
mkdir -p ./public;
cp ssg6 ./src/files/ssg6;
./ssg6 src public "Wilson's Blog" "https://wilsonjholmes.xyz"
./rssg public/index.html "Wilson's Blog" > public/rss.xml

#!/bin/sh -e

git submodule update --init

cd lowdown
./configure
make
sudo make install
cd ..

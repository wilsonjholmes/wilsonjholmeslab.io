#!/bin/bash

# delete all files in the files directory
rm -rf src/files/*

# get ".jpg" or ".JPG" file names from "files" and "files-full-size" directories
# and if there is not any difference between them do nothing
# otherwise convert and copy files from "files/full-size" to "files"
filesNames=$(find src/files -type f -name "*.jpg" -o -name "*.JPG" | sort)
fullSizeNames=$(find src/full-size-files -type f -name "*.jpg" -o -name "*.JPG" | sort)
if [ "$filesNames" = "$fullSizeNames" ]; then
    echo "No need to convert files"
else
    echo "Converting files"
    for file in $fullSizeNames; do
        convert -resize "1920x" -quality "80%" "$file" src/files/$(basename "$file")
    done
fi

# copy all other files (not .jpg or .JPG) from "files-full-size" to "files"
echo "Copying files"
for file in $(find src/full-size-files -type f -not -name "*.jpg" -not -name "*.JPG" | sort); do
    cp "$file" src/files/$(basename "$file")
done
